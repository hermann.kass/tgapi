from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly  
from django.shortcuts import get_object_or_404
from .serializers import (
    Region,
    Prefecture,
    Commune,
    Canton,
    Village,
    Quartier,
    Leader,

    RegionSerializer,
    PrefectureSerializer,
    CommuneSerializer,
    CantonSerializer,
    VillageSerializer,
    QuartierSerializer,
    LeaderSerializer
)
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import filters
from drf_yasg.utils import swagger_auto_schema

@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des régions",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class RegionViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de région.
    """
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']



@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des préfectures",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class PrefectureViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de préfecture.
    """
    
    queryset = Prefecture.objects.all()
    serializer_class = PrefectureSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']




@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des communes",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class CommuneViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de commune.
    """
    queryset = Commune.objects.all()
    serializer_class = CommuneSerializer
    permission_classes([IsAuthenticated])
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']


@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des cantons",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class CantonViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de canton.
    """
    queryset = Canton.objects.all()
    serializer_class = CantonSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']


@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des villages",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class VillageViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de village.
    """
    queryset = Village.objects.all()
    serializer_class = VillageSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']


@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des quartiers",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class QuartierViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments de quartier.
    """
    queryset = Quartier.objects.all()
    serializer_class = QuartierSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description']
    filtersets_fields = ['name','description']


@swagger_auto_schema(
        operation_description="Cette classe retourne la liste des dirigeants",
        responses={200:PrefectureSerializer,400 : "Bad request"}
    )
class LeaderViewSet(mixins.CreateModelMixin,mixins.ListModelMixin,mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    """
    Cette classe permet de lire, ecrire et mettre à jour les éléments les dirigeants.
    """
    queryset = Leader.objects.all()
    serializer_class = LeaderSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]