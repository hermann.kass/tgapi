


# Project : TGAPI
Where we build an open source API to retrieve some data about Togo

# Les Apps du projet
## Composition
Cette app met à la disposition des internautes les information sur les stuructures administratives du Togo. les Données sont disponibles à l'URL suivante : [kalamar.xyz/gouv/](https://kalamar.xyz/gouv/)


## Litterature
Cette app fourni un répertoire des oeuvres littéraires togolaises, africaines, ... Elle sera disponible à l'adresse suivante : [kalamar.xyz/litterature/](https://kalamar.xyz/litterature/) 

## Prenoms
Cette App receuil les prenoms, leurs explication ou sens, leurs origines. Elle sera disponible à l'adresse suivante : 
[kalamar.xyz/prenoms/](https://kalamar.xyz/prenoms/) 



# Contributors
- [hermannkass](https://gitlab.com/hermann.kass).

# Besoin d'aide
une fois déployé nous aurons besoins d'aide pour saisir les données.

![Image du jour](https://myoctocat.com/assets/images/octocats/octocat-23.png)
